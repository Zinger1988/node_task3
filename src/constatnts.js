const TRUCK_TYPES = [
  {
    name: 'SPRINTER',
    width: 250,
    height: 170,
    length: 300,
    loadCapacity: 1700,
  },
  {
    name: 'SMALL STRAIGHT',
    width: 250,
    height: 170,
    length: 500,
    loadCapacity: 2500,
  },
  {
    name: 'LARGE STRAIGHT',
    width: 350,
    height: 200,
    length: 700,
    loadCapacity: 4000,
  },
];

const ROLES = {
  DRIVER: 'DRIVER',
  SHIPPER: 'SHIPPER',
};

const LOAD_STATUSES = {
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
};

const LOAD_STATES = {
  UNSET: 'Unset',
  ROUTE_PICK_UP: 'En route to Pick Up',
  ARRIVED_PICK_UP: 'Arrived to Pick Up',
  ROUTE_DELIVERY: 'En route to delivery',
  ARRIVED_DELIVERY: 'Arrived to delivery',
};

const TRUCK_STATUSES = {
  ON_LOAD: 'OL',
  IN_SERVICE: 'IS',
};

function getTruckNames() {
  return TRUCK_TYPES.reduce((acc, cur) => {
    acc.push(cur.name);
    return acc;
  }, []);
}

module.exports = {
  ROLES,
  LOAD_STATUSES,
  LOAD_STATES,
  TRUCK_TYPES,
  TRUCK_STATUSES,
  getTruckNames,
};

const mongoose = require('mongoose');

const { Schema } = mongoose;
const { ROLES } = require('../constatnts');

const schema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      required: true,
      enum: Object.values(ROLES),
    },
    created_date: {
      type: String,
      required: true,
      default: new Date().toISOString(),
    },
    logs: [
      {
        message: {
          type: String,
          default: 'Unset',
        },
        time: {
          type: String,
          default: new Date().toISOString(),
        },
      },
    ],
  },
);

const User = mongoose.model('User', schema);

module.exports = User;

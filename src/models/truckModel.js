const mongoose = require('mongoose');

const { Schema } = mongoose;
const { TRUCK_STATUSES, getTruckNames } = require('../constatnts');

const schema = new Schema(
  {
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    type: {
      type: String,
      enum: getTruckNames(),
      required: true,
    },
    status: {
      type: String,
      enum: Object.values(TRUCK_STATUSES),
      default: TRUCK_STATUSES.IN_SERVICE,
      required: true,
    },
    created_date: {
      type: String,
      required: true,
      default: new Date().toISOString(),
    },
    logs: [
      {
        message: {
          type: String,
          default: 'Unset',
        },
        time: {
          type: String,
          default: new Date().toISOString(),
        },
      },
    ],
  },
);

const Truck = mongoose.model('Truck', schema);

module.exports = Truck;

const Truck = require('../models/truckModel');
const User = require('../models/userModel');
const throwError = require('../services/throwError');
const { TRUCK_STATUSES } = require('../constatnts');

const getTrucks = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'DRIVER') {
    throwError(400, 'e02');
  }

  const trucks = await Truck.find({ created_by: user._id });
  res.status(200).json({ trucks });
};

const addTruck = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'DRIVER') {
    throwError(400, 'e02');
  }

  const newTruck = new Truck({
    created_by: user._id,
    type: req.body.type,
    logs: {
      message: 'Truck was created',
      time: new Date().toISOString(),
    },
  });

  await newTruck.save();
  res.status(200).json({ message: 'Truck was successfully created' });
};

const getTruck = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'DRIVER') {
    throwError(400, 'e02');
  }

  const truck = await Truck.findOne({ truckId: req.params.id, created_by: user._id });

  if (!truck) {
    throwError(400, 'e03');
  }

  res.status(200).json({ truck });
};

const updateTruck = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'DRIVER') {
    throwError(400, 'e02');
  }

  const truck = await Truck.findOne({ _id: req.params.id, created_by: user._id });

  if (!truck) {
    throwError(400, 'e04');
  }

  if (truck.status === TRUCK_STATUSES.ON_LOAD) {
    throwError(400, 'e05');
  }

  await truck.updateOne({ type: req.body.type });
  res.status(200).json({ message: 'Truck details changed successfully' });
};

const deleteTruck = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'DRIVER') {
    throwError(400, 'e02');
  }

  const truck = await Truck.findOne({ _id: req.params.id, created_by: user._id });

  if (!truck) {
    throwError(400, 'e04');
  }

  if (truck.status === TRUCK_STATUSES.ON_LOAD) {
    throwError(400, 'e05');
  }

  await truck.deleteOne();
  res.status(200).json({ message: 'Truck deleted successfully' });
};

const assignTruckToDriver = async (req, res) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  const assignedTruck = await Truck.findOne({ assigned_to: userId });

  if (assignedTruck && String(assignedTruck._id) === truckId) {
    throwError(400, 'e09');
  }

  if (assignedTruck && assignedTruck.status === TRUCK_STATUSES.ON_LOAD) {
    throwError(400, 'e05');
  }

  if (assignedTruck) {
    await assignedTruck.updateOne({
      assigned_to: null,
      $push: {
        logs: {
          message: `Truck was unassigned from driver: ${assignedTruck.assigned_to}`,
          time: new Date().toISOString(),
        },
      },
    });
  }

  const truckToAssign = await Truck.findById(truckId);

  if (!truckToAssign) {
    throwError(400, 'e04');
  }

  await truckToAssign.updateOne({
    assigned_to: userId,
    $push: {
      logs: {
        message: `Truck was assigned to driver: ${userId}`,
        time: new Date().toISOString(),
      },
    },
  });

  res.status(200).json({ message: 'Truck assigned successfully' });
};

module.exports = {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruckToDriver,
};

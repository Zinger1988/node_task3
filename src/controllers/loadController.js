const User = require('../models/userModel');
const Load = require('../models/loadModel');
const Truck = require('../models/truckModel');
const {
  LOAD_STATES, LOAD_STATUSES, TRUCK_TYPES, TRUCK_STATUSES,
} = require('../constatnts');
const throwError = require('../services/throwError');

const getLoads = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  const { limit = 10, page = 0 } = req.query;
  const result = {};
  const startIndex = Number(page) * limit;
  const endIndex = (Number(page) + 1) * limit;

  if (user.role === 'SHIPPER') {
    result.total = await Load.countDocuments().exec();
    result.loads = await Load.find({ created_by: user._id })
      .skip(startIndex)
      .limit(limit);
  }

  if (user.role === 'DRIVER') {
    result.total = await Load.countDocuments({ assigned_to: user._id }).exec();
    result.loads = await Load.find({ assigned_to: user._id })
      .skip(startIndex)
      .limit(limit);
  }

  if (startIndex > 0 && result.total !== 0) {
    result.previous = {
      page: Number(page) - 1,
      limit,
    };
  }

  if (endIndex < result.total) {
    result.next = {
      page: Number(page) + 1,
      limit,
    };
  }

  res.status(200).json(result);
};

const addLoad = async (req, res) => {
  const user = await User.findById(req.user.userId);
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'SHIPPER') {
    throwError(400, 'e02');
  }

  const load = new Load({
    created_by: user._id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_date: new Date().toISOString(),
    logs: {
      message: `Load status changed to '${LOAD_STATUSES.NEW}'`,
      time: new Date().toISOString(),
    },
  });

  await load.save();
  res.status(200).json({ message: 'Load created successfully' });
};

const getActiveLoad = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'DRIVER') {
    throwError(400, 'e02');
  }

  const load = await Load.findOne({ assigned_to: user._id, status: 'ASSIGNED' });

  if (!load) {
    res.status(200).json({ message: 'No active loads' });
    return;
  }

  res.status(200).json({ load });
};

const switchLoadState = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'DRIVER') {
    throwError(400, 'e02');
  }

  const load = await Load.findOne({ assigned_to: user._id, status: 'ASSIGNED' });

  if (!load) {
    throwError(400, 'No data available');
  }

  const nextState = Object.values(LOAD_STATES)
    .filter((item, i, arr) => i !== 0 && arr[i - 1] === load.state).join();

  if (nextState) {
    await load.updateOne({
      $set: { state: nextState },
      $push: {
        logs: {
          message: `Load state changed to '${nextState}'`,
          time: new Date().toISOString(),
        },
      },
    });

    if (nextState === LOAD_STATES.ARRIVED_DELIVERY) {
      const truck = await Truck.findOne({ assigned_to: load.assigned_to });

      await truck.updateOne({ status: TRUCK_STATUSES.IN_SERVICE });
      await load.updateOne({
        assigned_to: null,
        status: LOAD_STATUSES.SHIPPED,
        $push: {
          logs: {
            $each: [
              {
                message: `Load status changed to '${LOAD_STATUSES.SHIPPED}'`,
                time: new Date().toISOString(),
              },
              {
                message: `Load was unassigned from driver: '${truck.assigned_to}'`,
                time: new Date().toISOString(),
              },
            ],
          },
        },
      });
    }

    res.status(200).json({ message: `Load state changed to '${nextState}'` });
  }
};

const getLoad = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  const load = await Load.findById(req.params.id);

  if (!load) {
    res.status(400).json({ message: 'Invalid load id' });
    return;
  }

  res.status(200).json({ load });
};

const updateLoad = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'SHIPPER') {
    throwError(400, 'e02');
  }

  const load = await Load.findById(req.params.id);

  if (!load) {
    throwError(400, 'e04');
  }

  if (load.status !== LOAD_STATUSES.NEW) {
    throwError(400, 'e05');
  }

  await load.updateOne(Object.assign(load, req.body));
  res.status(200).json({ message: 'Load details changed successfully' });
};

const deleteLoad = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'SHIPPER') {
    throwError(400, 'e02');
  }

  const load = await Load.findById(req.params.id);

  if (!load) {
    throwError(400, 'e04');
  }

  if (load.status !== LOAD_STATUSES.NEW) {
    throwError(400, 'e05');
  }

  await load.deleteOne();
  await res.status(200).json({ message: 'Load deleted successfully' });
};

const postLoad = async (req, res) => {
  const user = await User.findById(req.user.userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role !== 'SHIPPER') {
    throwError(400, 'e02');
  }

  const load = await Load.findById(req.params.id);

  if (!load) {
    throwError(400, 'e04');
  }

  await load.updateOne({
    status: LOAD_STATUSES.POSTED,
    $push: {
      logs: {
        message: `Load status changed to '${LOAD_STATUSES.POSTED}'`,
        time: new Date().toISOString(),
      },
    },
  });

  const truckTypes = [...TRUCK_TYPES].sort((a, b) => (a.loadCapacity >= b.loadCapacity ? 1 : -1));

  const matchedTruckType = truckTypes.find((truck) => {
    const truckParams = [
      truck.width,
      truck.height,
      truck.length,
      truck.loadCapacity,
    ];

    const loadParams = [
      load.dimensions.width,
      load.dimensions.height,
      load.dimensions.length,
      load.payload,
    ];

    return truckParams.every(((item, i) => item >= loadParams[i]));
  });

  if (!matchedTruckType) {
    throwError(400, 'e03');
  }

  const truck = await Truck.findOne({
    type: matchedTruckType.name,
    assigned_to: { $ne: null },
    status: TRUCK_STATUSES.IN_SERVICE,
  });

  if (truck) {
    await truck.updateOne({ status: TRUCK_STATUSES.ON_LOAD });
    await load.updateOne({
      status: LOAD_STATUSES.ASSIGNED,
      assigned_to: truck.assigned_to,
      state: LOAD_STATES.ROUTE_PICK_UP,
      $push: {
        logs: {
          $each: [
            {
              message: `Load assigned to driver: '${truck.assigned_to}'`,
              time: new Date().toISOString(),
            },
            {
              message: `Load status changed to '${LOAD_STATUSES.ASSIGNED}'`,
              time: new Date().toISOString(),
            },
          ],
        },
      },
    });
    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
    return;
  }

  await load.updateOne({
    status: LOAD_STATUSES.NEW,
    $push: {
      logs: {
        $each: [
          {
            message: 'Unable to find a truck for this load',
            time: new Date().toISOString(),
          },
          {
            message: `Load status changed to '${LOAD_STATUSES.NEW}'`,
            time: new Date().toISOString(),
          },
        ],
      },
    },
  });
  res.status(400).json({ message: 'No drivers to deliver your cargo' });
};

const getLoadInfo = () => {

};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  switchLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadInfo,
};

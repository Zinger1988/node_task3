const express = require('express');

const router = express.Router();
const asyncWrapper = require('../services/asyncWrapper');
const checkToken = require('../middleware/checkToken');

const {
  getLoads,
  addLoad,
  getActiveLoad,
  switchLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadInfo,
} = require('../controllers/loadController');

const { addLoadValidation } = require('../middleware/validationLoad');

router.get('/', checkToken, asyncWrapper(getLoads));
router.post('/', checkToken, addLoadValidation, asyncWrapper(addLoad));
router.get('/active', checkToken, asyncWrapper(getActiveLoad));
router.patch('/active/state', checkToken, asyncWrapper(switchLoadState));
router.get('/:id', checkToken, asyncWrapper(getLoad));
router.put('/:id', checkToken, asyncWrapper(updateLoad));
router.delete('/:id', checkToken, asyncWrapper(deleteLoad));
router.post('/:id/post', checkToken, asyncWrapper(postLoad));
router.get('/:id/shipping_info', checkToken, asyncWrapper(getLoadInfo));

module.exports = router;

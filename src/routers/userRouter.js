const express = require('express');

const router = express.Router();
const checkToken = require('../middleware/checkToken');
const asyncWrapper = require('../services/asyncWrapper');
const { passwordChangeValidation } = require('../middleware/validationUser');

const {
  getUserInfo,
  deleteUser,
  changeUserPassword,
} = require('../controllers/userController');

router.get('/', checkToken, asyncWrapper(getUserInfo));
router.delete('/', checkToken, asyncWrapper(deleteUser));
router.patch('/password', checkToken, passwordChangeValidation, asyncWrapper(changeUserPassword));

module.exports = router;

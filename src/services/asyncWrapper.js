const asyncWrapper = (controller) => async (req, res, next) => {
  try {
    return await controller(req, res, next);
  } catch (err) {
    next(err);
  }
};

module.exports = asyncWrapper;

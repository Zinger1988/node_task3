const errorMap = {
  e01: 'Not Authorized',
  e02: 'Access denied. Check role permissions',
  e03: 'No data available',
  e04: 'Invalid object id',
  e05: 'Access denied. Check object status',
  e06: 'Invalid email',
  e07: 'Invalid password',
  e08: 'Passwords do not match',
  e09: 'Truck was already assigned',
};

function throwError(status, code) {
  const error = { status, message: errorMap[code] || code };
  throw error;
}

module.exports = throwError;

const Joi = require('joi');

const signUpValidation = async (req, res, next) => {
  try {
    const schema = Joi.object({
      email: Joi
        .string()
        .email()
        .required(),
      password: Joi
        .string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required(),
      role: Joi
        .string()
        .required(),
    });

    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const signInValidation = async (req, res, next) => {
  try {
    const schema = Joi.object({
      email: Joi
        .string()
        .email()
        .required(),
      password: Joi
        .string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required(),
    });

    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

const passwordRecoveryValidation = async (req, res, next) => {
  try {
    const schema = Joi.object({
      email: Joi
        .string()
        .email()
        .required(),
    });

    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = {
  signUpValidation,
  loginValidation: signInValidation,
  passwordRecoveryValidation,
};

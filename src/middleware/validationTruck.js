const Joi = require('joi');
const { getTruckNames } = require('../constatnts');

const truckNames = getTruckNames();

const truckValidation = async (req, res, next) => {
  try {
    const schema = Joi.object({
      type: Joi
        .string()
        .valid(...truckNames)
        .required(),
    });
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = {
  truckValidation,
};

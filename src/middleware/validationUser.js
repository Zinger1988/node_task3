const Joi = require('joi');

const passwordChangeValidation = async (req, res, next) => {
  try {
    const schema = Joi.object({
      oldPassword: Joi
        .string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required(),
      newPassword: Joi
        .string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
        .required(),
    });

    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = { passwordChangeValidation };
